namespace eval Map {
  # Create a variable inside the namespace
  variable myResult
  namespace export PaintWorld
  namespace export ToPixel
  namespace export GenWorld
}
proc Map::PaintWorld2D {canvas list {offset 0} {startx 0} {dimension 0} {dhk 0}} {
#puts "hi $::dirt"
set px $startx
set pay 0
puts "PROP ($startx X $offset)"
for {set pay $offset} {$pay < [expr "$offset + 20"]} {incr pay} {
	set i [lindex $list $pay]
	#puts $i
	#puts $i
	foreach p $i {
		if 0 { if { $px > [expr "$startx+[Map::ToCoord [winfo width .]]"] } { break } }
		#puts "a [lindex $p 0]"
		#puts "$px * $pay"
		if { [lindex $p 0] eq "\$::air" } {
			#puts "air"
		} else {
		#puts [lindex [lindex $p 0] 0]
		$canvas create image [Map::ToPixel [expr "$px - $startx"]] [expr "[Map::ToPixel [expr "$pay - $offset"]] - $dhk"] -anchor nw -image [subst [lindex $p 0]]
		catch {
			$canvas create text [Map::ToPixel [expr "$px - $startx"]] [expr "[Map::ToPixel [expr "$pay - $offset"]] - $dhk"] -anchor nw -text [lindex $p 1] -fill white -font Courier
		}
		#[subst $[lindex $p 0]]
		}
		incr px
	}
	set px 0
}
}
proc Map::PaintWorld {canvas list {offset 0} {dimension 0} {doffset 0}} {
::callhook "worldpaint" [list $canvas $offset $dimension $doffset]
Map::PaintWorldNoHook $canvas $list $offset $dimension $doffset
}
proc Map::PaintWorldNoHook {canvas list {offset 0} {dimension 0} {doffset 0}} {
	$canvas delete all
	
	#catch {Map::PaintWorld2D $canvas [lindex $list [expr "$dimension - 1"]]} fid
	set dimension [expr "$dimension - 3"]
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 6
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 5
	incr dimension
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 4
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 3
	incr dimension
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 2
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 1
	incr dimension
	Map::PaintWorld2D $canvas [lindex $list $dimension] $doffset $offset $dimension 
	set ct 10
	set pa 0
	$canvas create rectangle 1 [expr "[winfo height .] - 45"] [winfo width .] [winfo height .] -fill gray
	foreach p $::inventory {

		set itemn "inv$pa"
		if { [ catch { lindex $p 1 } ] || [lindex $p 1] < 1 } {
			catch { lreplace ::inventory $pa $pa }
		} else {
		catch {
		$canvas create image $ct [expr "[winfo height .] - 40"] -anchor nw -tags $itemn -image [subst [lindex $p 0]]
		
		$canvas create text [expr "$ct + 5"] [expr "[winfo height .] - 30"] -text "[lindex $p 1]" -fill white -font Courier
		}
		}
		puts $pa
		incr pa
		set ct [expr "$ct + 36"]
	}
	$canvas create text 60 10 -text "Dimension: $dimension" -tags dinfo -font Courier
	$canvas create text 100 30 -text "( Click to Save )" -tags save -font Courier
	puts "PAINTWORLD $::copts(chat)"
	if { $::copts(chat) eq 1 } {
	#$canvas create rectangle 1 [expr "[winfo height .] - 300"] 200 [expr "[winfo height .] - 45"] -fill gray
	$canvas create text 60 [expr "[winfo height .] - 60"] -fill white -text "<Send Message>" -tags input -font Courier
	# thisi s really hacky!
	$canvas create text [expr "[winfo width .] / 2"] [expr "[winfo height .] / 2"] -fill white -width 400 -text $::copts(chatlog) -font Courier
	}
}

proc Map::SetWorldBlock2D {map w h bv} {
	set toret { }
	set tmpx { }
	for {set y 0} {$y < 64} {incr y} {
		if { $y == $h } {
			for {set x 0} {$x < 64} {incr x} {
				if { $x == $w } {
					lappend tmpx $bv
				} else {
					lappend tmpx [lindex [lindex $map $y] $x]
				}
			}
		lappend toret $tmpx

		} else {
			lappend toret [lindex $map $y]
		}
	}
	return $toret
}
proc Map::SetWorldBlock {mapy w h d bv} {
	::callhook "idle"
	::callhook "worldblock" [list $w $h $d $bv]
	if { [catch { set map $::testworld2 } ] } {
		set map $mapy
	}
	Map::SetWorldBlockNoHook $map $w $h $d $bv
}
proc Map::SetWorldBlockNoHook {map w h d bv} {
	set toret { }
	for {set z 0} {$z < 32} {incr z} {
		if { $d == $z } {
			lappend toret [Map::SetWorldBlock2D [lindex $map $z] $w $h $bv]
		} else {
			lappend toret [lindex $map $z]
		}
	}
	return $toret
}
proc Map::GenWorld2D {maxsky} {
	set toret { }
	for {set y 0} {$y < 64} {incr y} {
		if { $y < $maxsky } {
			set topick { \$::air }
		}
		if { $y >= $maxsky } {
			set topick $::validblocks
		}
		puts $topick
		set tmpx { }
		for {set x 0} {$x < 64} {incr x} {
			#set 
			lappend tmpx [randelem $topick]
		}
		lappend toret $tmpx
	}
	#puts $toret
	return $toret
	#puts $torets
}
proc Map::GenWorld {maxsky} {
	set toret { }
	for {set d 0} {$d < 256} {incr d} {
		lappend toret [Map::GenWorld2D $maxsky]
	}
	return $toret
}
proc lrandom L {
    lindex $L [expr {int(rand()*[llength $L])}]
} ;#RS
proc randelem {list} {
    lindex $list [expr {int(rand()*[llength $list])}]
}
proc Map::ToPixel {n} {
	return [expr "$n * 32"]
}
proc Map::ToCoord {n} {
	return [expr "round($n / 32)"]
}
proc Map::GetNode {list w h d {giveair 0}} {
	if {$giveair eq 1 } {
		return [lindex [lindex [lindex $list $d] $h] $w]
	}
	if { [lindex [lindex [lindex [lindex $list $d] $h] $w] 0] in {\$::air \$::water \$::door} } {
		return 0
	} else {
		return [lindex [lindex [lindex $list $d] $h] $w]
	}
}
proc Map::HasNode {list w h d} {
	if { [lindex [lindex [lindex [lindex $list $d] $h] $w] 0] in {\$::air \$::water \$::door} || [lindex [lindex [lindex [lindex $list $d] $h] $w] 1] eq 1 } {
		return 0
	} else {
		return 1
	}
}