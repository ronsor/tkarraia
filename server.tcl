package require http
set ip.appspot.com [http::data [http::geturl "http://myip.dnsomatic.com"]]
 # conf here
 set name "Server"; #Servername, no spaces
 set ip ${ip.appspot.com}; #Auto-get ip from ip.appspot.com
 set canfly 1; #Allow players to fly? (you can always override this with /set fly 1)
               # valid settings: -1 (never), 0 (off), 1 (on)
 set public 1; #Make this a public 
 set damage 10; # Doesn't do anything... yet
 # end conf
 if { $public eq 1 } {
	puts "IP: $ip"
	http::geturl "http://ronsor.net/2dtestserver.php?server=$name&ip=$ip"
 }
 global tpl
 global tplx
 # no-require-do-tolerate zlib
 global mymap
 proc readfile {file} {
set fd [open $file r]
set list [read $fd]
close $fd
return $list
}
namespace eval Map {}
proc Map::SetWorldBlockNoHook {map w h d bv} {
	set toret { }
	for {set z 0} {$z < 32} {incr z} {
		if { $d == $z } {
			lappend toret [Map::SetWorldBlock2D [lindex $map $z] $w $h $bv]
		} else {
			lappend toret [lindex $map $z]
		}
	}
	return $toret
}
proc Map::SetWorldBlock2D {map w h bv} {
	set toret { }
	set tmpx { }
	for {set y 0} {$y < 64} {incr y} {
		if { $y == $h } {
			for {set x 0} {$x < 64} {incr x} {
				if { $x == $w } {
					lappend tmpx $bv
				} else {
					lappend tmpx [lindex [lindex $map $y] $x]
				}
			}
		lappend toret $tmpx

		} else {
			lappend toret [lindex $map $y]
		}
	}
	return $toret
}
set ::tpl ""
set ::tplx ""
set ::mymap [string map {\\\$ \$} [readfile "[lindex $::argv 0]"]]
 proc Serve {chan addr port} {
    fconfigure $chan -translation auto -buffering line
    set line [gets $chan]
    set path [file join . [string trimleft [lindex $line 1] /]]
    if { $path == "." } {set path ./index.html}
	puts "$addr $path"
    if { $path == "./reload" } {
        set reload 1
        source [info script]
        puts $chan "HTTP/1.0 200 OK"
        puts $chan "Content-Type: text/html"
        puts $chan ""
        puts $chan "Server reloaded"
    } else {
       #puts "Request: $path"                ;##
        switch -glob -- "$path" {
			./map {
				puts $chan "HTTP/1.0 200 OK"
				puts $chan "Content-Type: text/x-tcl-list-map"
				puts $chan ""
				puts "Got client ask for map"
				puts $chan $::mymap
			}
			
			./setblock* {
				puts $chan "HTTP/1.0 200 OK"
				puts $chan "Content-Type: text/x-2dtest-status"
				puts $chan ""
				puts $chan "[join $::tplx ";"]"
				catch { set ::tpl [urlDecode [lindex [split $path "?"] 1]]; }
				puts $::tpl
				if { [llength $::tplx] > 25 } {
					set ::tplx ""
				}
					lappend ::tplx [list "setblock" {*}$::tpl]
					set ::mymap [Map::SetWorldBlockNoHook $::mymap {*}$::tpl] ;# we support tcl 8.6!
					swrite $::mymap [lindex $::argv 0]
			}
			./msg* {
				puts $chan "HTTP/1.0 200 OK"
				puts $chan "Content-Type: text/x-2dtest-status"
				puts $chan ""
				puts $chan "[join $::tplx ";"]"
				catch { set ::tpl [lindex [split $path "?"] 1]; set ::tpl [linsert $::tpl 0 "msg"]; lappend ::tplx $::tpl }
				#puts $::tpl
				if { [llength $::tplx] > 25 } {
					set ::tplx ""
				}
			}
			./status {
				puts $chan "HTTP/1.0 200 OK"
				puts $chan "Content-Type: text/x-2dtest-status"
				puts $chan ""
				#puts $chan [string map {\\\$ \$} [readfile "[lindex $::argv 0].msb"]]
				puts $chan "[join $::tplx ";"];fly $::canfly"
				#puts $::tpl
				if { [llength $::tplx] > 25 } {
					set ::tplx ""
				}
			}
		}
    }
        close $chan
 }

proc swrite {list file} {
set fd [open $file w]; puts $fd [string map {\$ \\\$} $list]; close $fd
}
proc urlDecode {str} {
    set specialMap {"[" "%5B" "]" "%5D"}
    set seqRE {%([0-9a-fA-F]{2})}
    set replacement {[format "%c" [scan "\1" "%2x"]]}
    set modStr [regsub -all $seqRE [string map $specialMap $str] $replacement]
    return [encoding convertfrom utf-8 [subst -nobackslash -novariable $modStr]]
} 
 #catch {console show}                ;##
 if { ! [info exists reload] } {
    set sk [socket -server Serve 30004]
    vwait forever
 } else {
    unset reload
 }
