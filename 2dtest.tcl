#global debugMode
global inventory
global cur
global copts
global can
set copts(name) "user"
catch { source config.tcl }
proc listns {{parentns ::}} {
    set result [list]
    foreach ns [namespace children $parentns] {
        lappend result {*}[listns $ns] $ns
    }
    return $result
}
proc ::callhook {hookname {params ""}} {
	foreach y [listns] {
		#puts "${y}::${hookname}"
		if { [string length [namespace which ${y}::${hookname}]] > 0 } {
		if { [string length $params] > 0 } {
			set params "{ $params }"
		}
		 eval "${y}::${hookname} $params" 
		}
	}
}
global testworld2
set inventory { {\$::dirt 99} }
set cur 0
global po
set po 0
global do
set do 25
 proc splitline {string blah} {
        set rc [llength [split $string $blah]] 
        incr rc -1
        return $rc
 }
proc value_dialog {string title} {
    set w [toplevel .[clock seconds]]
    wm resizable $w 0 0
    wm title $w $title
    label  $w.l -text $string
    entry  $w.e -textvar $w -bg white -width 30
    bind $w.e <Return> {set done 1}
    button $w.ok     -text OK     -command {set done 1}
    button $w.c      -text Clear  -command "set $w {}"
    button $w.cancel -text Cancel -command "set $w {}; set done 1"
    grid $w.l  -    -        -sticky news
    grid $w.e  -    -        -sticky news
    grid $w.ok $w.c $w.cancel
    vwait done
    destroy $w
    set ::$w
}
proc ::PromptInput {string} {
	value_dialog $string "Called by external script"
}
proc menu {canvas} {
	set theheight [winfo height .]
	set thewidth [winfo width .]
	if { $theheight eq 1 } {
		set theheight 480
		set thewidth 810
	}
	set startpos [expr "$theheight / 2 - 50"]
	$canvas create rectangle 0 $startpos $thewidth [expr "$startpos + 100"] -fill gray
	$canvas create text 60 [expr "$startpos + 25"] -text "2DTest menu" -tags dinfo -font Courier
	$canvas create text 60 [expr "$startpos + 80"] -text "<Open>" -tags open -font Courier
	$canvas create text 120 [expr "$startpos + 80"] -text "<New>" -tags new -font Courier
	::callhook "menu" [list $canvas $startpos]
}
catch { console show } fid
proc facing {facing} {
	puts $facing
	if { $facing == "$::DPLUS" } {
		set ret \$::headfront
	}
	if { $facing == "$::DMIN" } {
		set ret \$::headback
	}
	if { $facing == "$::FPLUS" } {
		set ret \$::headright
	}
	if { $facing == "$::FMIN" } {
		set ret \$::headleft
	}

	return $ret
}
#global dirt
set DPLUS 3
set DMIN 2
set FPLUS 4
set FMIN 1
set facing $FPLUS
set fly 1
global facing
global DPLUS
global DMIN
global FPLUS
global FMIN
global PASSTHROUGH
global px
global py
global pz
set px 2
set py 30
set pz 1
set PASSTHROUGH 1
global ver
global validblocks
set validblocks {\$::dirt \$::dirt \$::water \$::stone \$::stone}
set old [list "\$::air"]
package require Tk
catch { cd \\freewra } fid
source libimg.tcl
source libmap.tcl
set ver "2.0-beta"
set copts(mods) [glob -nocomplain -type d "mods/*"]
puts $copts(mods)
set copts(fly) 0
set copts(op) 1
set copts(chat) 0
set copts(chatlog) ""
global water, dirt, headback, headfront, headleft, headright, stone
set ::dirt [newnode "dirt.gif"]
set ::headback [newnode "headback.gif" 1]
set ::headfront [newnode "headfront.gif" 1]
set ::headleft [newnode "headleft.gif" 1]
set ::headright [newnode "headright.gif" 1]
set ::door [newnode "door.gif" 1]
set ::chest [newnode "chest.gif"]
set ::stone [newnode "stone.gif"]
set ::water [newnode "water.gif" {$PASSTHROUGH}]
puts $::dirt
set ::air ""
puts $::air
#grid ttk::button .b -text "Hello World"
wm geometry . "800x480+0+0"
wm title . "2DTest ($::ver)"
set can [canvas .can -height 480 -width 640 -bg skyblue]
puts "OK"
foreach modname $copts(mods) {
	source "$modname/init.tcl"
}
proc every {ms body} {
    if 1 $body
    after $ms [list after idle [info level 0]]
}
every 1000 { ::callhook "idle" }
::callhook "init"
#.can create image 0 0 -anchor nw -image $dirt
pack .can -expand 1 -fill both
menu $can
bind . <Key-Right> {
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	if { $facing > 3 } {
	set facing 0
	return
	}
	set facing [expr "$facing + 1"]
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	puts $facing
	Map::PaintWorld $can $testworld2 $po $pz $do
}
bind . <Key-Left> {
set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	if { $facing < 2 } {
	set facing 5
	}
	set facing [expr "$facing - 1"]
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
}
bind . <Key-Up> {
	puts "up"
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz $old ]
	if { $facing == $DPLUS } {
		puts "MATCH $facing"
		set pz [expr "$pz + 1"]
	}
	if { $facing == $DMIN } {
		puts "MATCH $facing"		
		set pz [expr "$pz - 1"]
	}
	if { $facing == $FPLUS } {
				puts "MATCH $facing $pz"
				set px [expr "$px + 1"]
				incr po
	}
	if { $facing == $FMIN } {
				puts "MATCH $facing "
				set px [expr "$px - 1"]
				incr po -1
	}
		set posen [gravity $px $py $pz]
	set px [lindex $posen 0]
	set py [lindex $posen 1]
	set pz [lindex $posen 2]
	set ::old [Map::GetNode $testworld2 $px $py $pz 1]
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
	puts $pz
}
bind . <Key-Down> {
	puts "down"
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz $::old ]
	if { $facing == $DPLUS } {
		set pz [expr "$pz - 1"]
	}
	if { $facing == $DMIN } {
		set pz [expr "$pz + 1"]
	}
	if { $facing == $FPLUS } {
		set px [expr "$px - 1"]
		incr po -1
	}
	if { $facing == $FMIN } {
		set px [expr "$px + 1"]
		incr po 1
	}
	set posen [gravity $px $py $pz]
	set px [lindex $posen 0]
	set py [lindex $posen 1]
	set pz [lindex $posen 2]
	set ::old [Map::GetNode $testworld2 $px $py $pz 1]
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
	
}
proc gravity {rx ry rz} {
	set cyc 0
	if { 1 > $::copts(fly) } {
		if { [Map::HasNode $::testworld2 $rx [expr "$ry + 1"] $rz ] eq 1 } {
			incr rx -1
			incr ::po -1
		}
	while { 1 } {
		puts "CYCLES: $cyc, $rx, $ry, $rz"
		if { [Map::GetNode $::testworld2 $rx [expr "$ry + 2"] $rz 1] != "\$::air" } {
		::callhook "gravity" $cyc
		return [list $rx $ry $rz]
		} else {
		incr ry 1
		}
		incr cyc
	}
	} else {
		return [list $rx $ry $rz]
	}
}
bind . <Key-space> {

	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	incr py -1
		set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
	if { $::copts(fly) < 1 } {
	after 500 {
	if { [Map::HasNode $testworld2 $px [expr "$py + 2"] $pz] eq 0 } {
set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	incr py 1

		set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
	}
	}
	}
}
bind .can <1> {
		set xx %x
		set yy %y
		puts "$yy Y $xx X"
	    set x [.can canvasx $xx] ; set y [.can canvasy $yy]
        set i [.can find closest $x $y]
        set t [.can gettags $i]
		puts "$t T $i I"
		switch -glob -- [lindex [split $t " "] 0] {
			open {
			set savefile [value_dialog "Worldname:" "Open Saved World"]
			append savefile ".map"
			set testworld1 [string map {\\\$ \$} [readfile "$savefile"]]
			set testworld2 [Map::SetWorldBlock $testworld1 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
			Map::PaintWorld $can $testworld2 $po $pz $do
			}
			new {
			set savefile ""
			set testworld1 [Map::GenWorld 32]
			set testworld2 [Map::SetWorldBlock $testworld1 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
			Map::PaintWorld $can $testworld2 $po $pz $do
			}
			save {
				if { $savefile eq "" } {
				set savefile [value_dialog "Desired Worldname:" "Save World"]
				append savefile ".map"
				}
				save $testworld1 $savefile
			}
			inv? {
				puts [string index $t 3]
				set cur [string index $t 3]
			}
			input {
				if {[splitline $copts(chatlog) "\n"] > 6} {
					set copts(chatlog) ""
				}
				set msg [value_dialog "Message or command:" "Chat"]
				::callhook "playermsg" [list "$::copts(name)" "$msg"]
				append copts(chatlog) "$msg\n"
				if {[string index $msg 0] eq "/"} {
					set mopts [split $msg]
					if {[lindex $mopts 0] eq "/set"} {
					if {[catch {lindex $mopts 1}]} {
						foreach name [array names copts] {
						append copts(chatlog) "$name=$copts($name)\n"
						}
					} else {
					set copts([lindex $mopts 1]) [lindex $mopts 2]
					}
					}
					if {[lindex $mopts 0] eq "/config"} {
					foreach name [array names copts] {
					puts $name
						append copts(chatlog) "$name=$copts($name)\n"
						}
					}
					switch -- [lindex $mopts 0] {
						/nick {
							set oldname $::copts(name)
							set copts(name) [lindex $mopts 1]
							::callhook "playermsg" [list "$oldname" "is now known as $::copts(name)"]
						}
						/about {
							append copts(chatlog) "2DTest $::ver\n"
						}
						/mods {
							append copts(chatlog) "$copts(mods)\n"
						}
						/giveme {
							lappend ::inventory [list "\$::[lindex $mopts 1]" "99"]
						}
						/help {
							Chat::Print "/about - print about info"
							Chat::Print "/mods - list mods"
							Chat::Print "/giveme <nodename> - give yourself an item"
							Chat::Print "/config - list c-opts."
							Chat::Print "/set <c-opt> <value> - sets a c-opt"
							Chat::Print "/nick <newname> - set your name (only really matters with mod_multiplayer)"
						}
						/clear {
							set copts(chatlog) ""
						}
						default {
							::callhook "cmd_[string trimleft [lindex $mopts 0] "/"]" "$mopts"
						}
					}
				}
				Map::PaintWorld $can $testworld2 $po $pz $do
			}
			default {
				callhook "menu_[lindex [split $t " "] 0]"
				if { [info exists testworld2] && [Map::HasNode $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz] eq 1 } {
					set psa 0
					set pax 0
					puts "Y"
					foreach ia $::inventory {
						if { [lindex $ia 0] eq [Map::GetNode $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz] && [lindex $ia 1] < 99} {
							lset ::inventory $pax [list [lindex [lindex $::inventory $pax] 0] [expr "[lindex [lindex $::inventory $pax] 1] + 1"] ]
							puts $::inventory
							puts "YAY"
							set psa 1
						}
						incr pax
					}
					if { $psa eq 0 } {
						lappend ::inventory [list [Map::GetNode $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz] 1]
					}
					set testworld2 [Map::SetWorldBlock $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz \$::air]
					Map::PaintWorld $can $testworld2 $po $pz $do
				}
			}
		}
}
bind .can <3> {
		set xx %x
		set yy %y
		puts "$yy Y $xx X"
	    set x [.can canvasx $xx] ; set y [.can canvasy $yy]
        set i [.can find closest $x $y]
        set t [.can gettags $i]
			if { [catch {lindex $::inventory $::cur}] } {
			} else {
				if { [info exists testworld2] } {
					if { [lindex [lindex $::inventory $::cur] 1] > -1 } {
					if { [Map::HasNode $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz] eq 0 } {
					set testworld2 [Map::SetWorldBlock $testworld2 [expr "[Map::ToCoord $x] + $po"] [expr "[Map::ToCoord $y] + $do"] $pz [lindex [lindex $::inventory $::cur] 0]]
					Map::PaintWorld $can $testworld2 $po $pz $do
					lset ::inventory $::cur [list [lindex [lindex $::inventory $::cur] 0] [expr "[lindex [lindex $::inventory $::cur] 1] - 1"] ]
					}
					} else {
					catch { lreplace ::inventory $::cur $::cur }
					}
				}
			}
}
bind . <u> {
if { $::copts(fly) eq 1 } {
set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	incr py -1
	incr do -1
	set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
}
}
bind . <d> {
	if { [Map::HasNode $testworld2 $px [expr "$py + 2"] $pz] eq 0} {
set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz \$::air ]
	incr py 1
	incr do 1
		set testworld2 [Map::SetWorldBlock $testworld2 $px $py $pz [list [facing $facing] "$::copts(name)"] ]
	Map::PaintWorld $can $testworld2 $po $pz $do
}
}
bind . <f> {
	if { $::copts(fly) != -1 } {
		if { $::copts(fly) == 0 } {
			set n 1
		} else {
			set n 0
		}
		set ::copts(fly) $n
	}
}
bind . <c> {
	callhook "idle"
	puts "C-activated"
	if { $::copts(chat) eq 1 } {
		puts "Chas em"
		set ::copts(chat) 0
		} else {
		puts "YAY2++"
		set ::copts(chat) 1
	}
	Map::PaintWorld $can $testworld2 $po $pz $do
}
bind . <Configure> {
	if { [info exists testworld2] } {
	callhook "idle"
	Map::PaintWorld $can $testworld2 $po $pz $do
	}
}
proc save {list file} {
set fd [open $file w]; puts $fd [string map {\$ \\\$} $list]; close $fd
}
proc readfile {file} {
set fd [open $file r]
set list [read $fd]
close $fd
return $list
}
namespace eval Chat {
	variable blah
}
proc Chat::Print {text} {
	append ::copts(chatlog) "$text\n"
}