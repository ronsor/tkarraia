namespace eval Damage { }
set ::copts(damage) 10
proc Damage::init {} {
	set ::copts(damage) 10
	set ::opz $::pz
	set ::opx $::px
	set ::opy $::py
}
proc Damage::idle {} {
		if { $::copts(damage) > -10000 && [info exists ::testworld2] } {
		.can delete damagecounter
		.can create text 100 50 -fill red -text "Health: $::copts(damage)" -tags damagecounter -font Courier
		if { $::copts(damage) < 1 } {
			set ::copts(damage) 10
			after 1000 { destroy .dialog1 }
			tk_dialog .dialog1 "Damage" "You died. Move in any direction to respawn." info 0 "Please Wait.."
			set ::pz $::opz
			set ::px $::opx
			set ::py $::opy
			set ::po 0
		}
		}
}
proc Damage::cmd_gamemode {{args ""}} {
	if { "[lindex $args 1]" eq "1" } {
		Chat::Print "Damage enabled."
		set ::copts(damage) 10
	} else {
		Chat::Print "Damage disabled."
		set ::copts(damage) -10000
	}
}
proc Damage::gravity {depth} {
	if { $depth < 3 } {
		return 0
	}
	set ::copts(damage) [expr "$::copts(damage) - $depth + 3"]
}