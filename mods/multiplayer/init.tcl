namespace eval Multiplayer {variable httpuri}
package require http
package require zlib
proc Multiplayer::init {} {
	puts "Multiplayer loaded"
	
}
proc Multiplayer::cmd_server {mopts} {
	set ::Multiplayer::httpuri [lindex $mopts 1]
	Chat::Print "Connecting to [lindex $mopts 1]:30004"
	 after 3000 {destroy .dialog1}
	tk_dialog .dialog1 "Multiplayer" "Connecting..." info 0 OK
	puts "CON"
	set ::testworld2 [http::data [http::geturl "http://${::Multiplayer::httpuri}:30004/map"]]
	#set ::Multiplayer::chan [socket [lindex $mopts 1] 30004]         ;# Open the connection
	Map::PaintWorld $::can $::testworld2 $::po $::pz $::do
	puts "DONE"
	#fileevent $::Multiplayer::chan readable \
         [list Multiplayer::gotmsg $chan]   
}
proc stripchars {str chars} {
    foreach c [split $chars ""] {set str [string map [list $c ""] $str]}
    return $str
}
proc Multiplayer::menu {canvas} {
[lindex $canvas 0] create text 260 [expr "[lindex $canvas 1]+ 80"] -text "<Connect to Server>" -tags server -font Courier
[lindex $canvas 0] create text 430 [expr "[lindex $canvas 1]+ 80"] -text "<Server List>" -tags serverlist -font Courier
}
proc Multiplayer::menu_serverlist {} {
	tk_dialog .dialog1 "Multiplayer" "Server List:\n[http::data [http::geturl "http://ronsor.net/servers.txt"]]" info 0 "Ok"
}
proc Multiplayer::menu_server {} {
Multiplayer::cmd_server [list "server" [::PromptInput "Enter hostname/IP:"]]
}
proc Multiplayer::worldblock {mopts} {
	puts "WORLDBLOCK YAY"
	if { [info exists ::Multiplayer::httpuri] } {
	if { [lindex $mopts 0] eq "status" } {
		puts "Status :)"
		set status [http::data [http::geturl "http://${::Multiplayer::httpuri}:30004/status"]]
	} else {
		puts "REAL :)"
		set status [http::data [http::geturl "http://${::Multiplayer::httpuri}:30004/setblock?[urlEncode "$mopts"]"]]
	}
	puts $status
	foreach staty [split $status ";"] {
		puts "$staty"
		set stat [urlDecode [split $staty]]
		puts "$stat"
		if { [lindex $stat 0] eq "setblock" } {
			#puts "a setblock"
			catch { set ::testworld2 [Map::SetWorldBlockNoHook $::testworld2 [lindex $stat 1] [lindex $stat 2] [lindex $stat 3] "[stripchars [lindex $stat 4] "{}"]" ] }
		}
		if { [lindex $stat 0] eq "msg" } {
			puts "$stat"
			if { [string match "*[lindex $stat 2]*" $::copts(chatlog) ] } {
			
			} else {
			Chat::Print "<[lindex $stat 1]> [lindex $stat 2]"
			}
		}
		if { [lindex $stat 0] eq "fly" } {
			#Chat::Print "-!- Option 'fly' is [lindex $stat 1]"
			set ::copts(fly) [lindex $stat 1]
		} 
		if { [lindex $stat 0] eq "damage" } {
			set ::copts(damage) [lindex $stat 1]
		}
	}
	#Map::PaintWorld $::can $::testworld2 $::po $::pz $::do
	}
}
proc Multiplayer::cmd_/ {mopts} {
	::callhook "playermsg" [list $::copts(name) [join $mopts " "]]
}
proc Multiplayer::idle {} {
	puts "im idle"
	Multiplayer::worldblock [list status]
}
proc Multiplayer::playermsg {msg} {
	if { [info exists ::Multiplayer::httpuri] } {
	set status [http::data [http::geturl "http://${::Multiplayer::httpuri}:30004/msg?[urlEncode "$msg"]"]]
	Multiplayer::idle
	}
}
proc Multiplayer::cmd_exit {mopts} {
	close $::Multiplayer::chan
	exit
}
proc urlDecode {str} {
    set specialMap {"[" "%5B" "]" "%5D"}
    set seqRE {%([0-9a-fA-F]{2})}
    set replacement {[format "%c" [scan "\1" "%2x"]]}
    set modStr [regsub -all $seqRE [string map $specialMap $str] $replacement]
    return [encoding convertfrom utf-8 [subst -nobackslash -novariable $modStr]]
}
proc urlEncode {str} {
    set uStr [encoding convertto utf-8 $str]
    set chRE {[^-A-Za-z0-9._~\n]};		# Newline is special case!
    set replacement {%[format "%02X" [scan "\\\0" "%c"]]}
    return [string map {"\n" "%0A"} [subst [regsub -all $chRE $uStr $replacement]]]
}